/*== alice.swal2.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class SWALController {
    constructor(alice) {
        this._vm = alice;
    }

    success(message, title) {
        return Swal.fire(
            title,
            message,
            'success'
        );
    };
    info(message, title) {
        return Swal.fire(
            title,
            message,
            'info'
        );
    };
    error(message, title) {
        return Swal.fire(
            title,
            message,
            'error'
        );
    };
    warn(message, title, btnOK, btnCancel) {
        return Swal.fire({
            title: title || "",
            text: message || "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: btnOK || 'Yes',
            cancelButtonText: btnCancel || 'No',
        });
    };
}

$a.swal = new SWALController($a);