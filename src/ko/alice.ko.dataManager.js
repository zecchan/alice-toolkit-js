/*== alice.ko.dataManager.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class AliceModelManager {
    constructor(alice) {
        this._vm = alice;
    }

    add(name, model, addToData) {
        if (typeof name !== "string") {
            vm.debugError("A model name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            vm.debugError("A model named [" + name + "] already exists");
            return;
        }
        if (typeof model !== "object") {
            vm.debugError("A model must be an object, " + typeof model + " given");
            return;
        }
        this[name] = new AliceModelRecord(model);
        if (addToData) {
            this._vm.data.add(name, this[name]);
        }
    }
}

class AliceModelRecord {
    constructor(mdl) {
        this.base = mdl;
        this.isModel = true;
    }

    map() {
        var re = ko.mapping.fromJS(this.base);
        re._empty = this.empty.bind(this, re);
        re._unmap = this.unmap.bind(this, re);
        re._fill = this.fill.bind(this, re);
        return re;
    }

    empty(data) {
        ko.mapping.fromJS(this.base, data);
    }

    unmap(data) {
        return ko.mapping.toJS(data);
    }

    fill(data, record) {
        ko.mapping.fromJS(record, data);
    }
}

class AliceDataManager {
    constructor(alice) {
        this._vm = alice;
    }

    add(name, data) {
        if (typeof name !== "string") {
            console.error("A data name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            console.error("A data named [" + name + "] already exists");
            return;
        }
        if (Array.isArray(data)) {
            this[name] = ko.observableArray(data);
        }
        else if (data.isModel) {
            this[name] = data.map();
        }
        else {
            if (typeof data == "object")
                this[name] = ko.mapping.fromJS(data);
            else
                this[name] = ko.observable(data);
        }
    }

    fromModel(mname, name) {
        if (!name)
            name = mname;
        if (typeof name !== "string") {
            console.error("A data name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            console.error("A data named [" + name + "] already exists");
            return;
        }
        if (this._vm.models[mname] && typeof this._vm.models[mname] === "object") {
            this[name] = this._vm.models[mname].map();
        }
    }
}

$a.models = new AliceModelManager($a);
$a.data = new AliceDataManager($a);
$a.applyKnockoutBindings = function() {
    ko.applyBindings($a.data);
}