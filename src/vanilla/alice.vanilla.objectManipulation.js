/*== alice.vanilla.objectManipulation.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};
$a.combineObject = function(a, b) {
    if (typeof a != 'object' || typeof b != "object")
        throw new Error("Both parameter must be an object");
    var res = $a.deepClone(a) || {};
    if (!b) return res;
    for(var k in b) {
        if (!res[k])
            res[k] = $a.deepClone(b[k]);
    }
    return res;
}
$a.shallowClone = function(a) {
    if (typeof a == "object") {
        if (Array.isArray(a)) {
            var r = [];
            for(var i = 0; i < a.length; i++)
                r.push(a[i]);
            return r;
        } 
        else if (a) {
            var r = {};
            for(var k in a) 
                r[k] = a[k];
            return r;
        }
        else
            return a;
    }
    return a;
}
$a.clone = function(a) {
    return $a.shallowClone(a);
}
$a.deepClone = function(a) {
    if (typeof a == "object") {
        if (Array.isArray(a)) {
            var r = [];
            for(var i = 0; i < a.length; i++)
                r.push($a.deepClone(a[i]));
            return r;
        } 
        else if (a) {
            var r = {};
            for(var k in a) 
                r[k] = $a.deepClone(a[k]);
            return r;
        }
        else
            return a;
    }
    return a;
}