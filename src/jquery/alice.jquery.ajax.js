/*== alice.jquery.ajax.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class AliceCRUDManager {
    constructor(mgr) {
        this._vm = mgr._vm;
        this._mgr = mgr;
        this._form = null;

        this.msgSaveSuccess = 'Saved successfully!';
        this.msgSaveFailed = 'Save failed!';
        this.msgDeleteSuccess = 'Deleted successfully!';
        this.msgDeleteFailed = 'Delete failed!';
    }

    validate(form) {
        form = form || this._form;
        var validator = $(form).kendoValidator().data("kendoValidator");
        return validator.validate();
    }

    setForm(form) {
        this._form = form;
    }

    save(url, data, skipValidation) {
        if (!skipValidation && !this.validate())
            return;
        
        if (data.isModel)
            data = data._unmap();
        this._mgr.post(url, data).then(function(res) {
            console.log(res);
        });
    }
}

class AliceAJAXManager {
    constructor(alice) {
        this._vm = alice;
        this.debug = false;
    };

    _pool = [];
    _id = 1;

    send(options) {
        var rec = new AliceAJAXRecord(options, this);
        rec.ID = this._id++;

        rec.start();
        $.ajax(options).then(
            function (data, textStatus, jqXHR) {
                this.done(data, textStatus, jqXHR);
            }.bind(rec),
            function (jqXHR, textStatus) {
                try {
                    this.done(JSON.parse(jqXHR.responseText), textStatus, jqXHR);
                }
                catch {
                    this.done(jqXHR.responseText, textStatus, jqXHR);
                }
            }.bind(rec)
        );

        this._pool.push(rec);
        return rec;
    };

    get(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'GET'
        };
        return this.send(opt);
    }

    post(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'POST'
        };
        return this.send(opt);
    }

    postJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'POST'
        };
        return this.send(opt);
    }

    postFormData(url, form) {
        var opt = {
            url: url,
            data: form,
            contentType: 'application/json; charset=utf-8',
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST'
        };
        return this.send(opt);
    }

    patch(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'PATCH'
        };
        return this.send(opt);
    }

    patchJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'PATCH'
        };
        return this.send(opt);
    }

    delete(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'DELETE'
        };
        return this.send(opt);
    }

    deleteJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'DELETE'
        };
        return this.send(opt);
    }
}

class AliceAJAXRecord {
    constructor(opt, mgr) {
        this.ID = -1;
        this.Url = opt.url;
        this.Method = opt.method;
        this.Initiated = new Date();
        this.Data = opt.rawData || opt.data;
        this._manager = mgr;
    }

    start() {
        this.Initiated = new Date();
        vm.debugInfo("Ajax request #" + this.ID + " is started: " + this.Method + " " + this.Url, this.Data);
    }

    then(f) {
        this._after = f;
    }

    done(data, textStatus, jqXHR) {
        if (typeof this._after === "function") {
            if (textStatus == "error")
                vm.debugError("Ajax request #" + this.ID + " error: " + this.Method + " " + this.Url, data);
            else {
                var el = new Date() - this.Initiated;
                vm.debugSuccess("Ajax request #" + this.ID + " is completed (" + el + "ms elapsed): " + this.Method + " " + this.Url, data);
            }
            this._after(data, textStatus, jqXHR);
        }
    }

    remove() {
        var ix = this._manager._pool.indexOf(this);
        if (ix >= 0)
            this._manager._pool.splice(ix, 1);
    }
}

$a.ajax = new AliceAJAXManager($a);
$a.crud = new AliceCRUDManager($a.ajax);