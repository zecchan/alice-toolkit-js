/*== alice.vanilla.collection.js by Code Atelier (c) 2021 ==*/
Array.prototype.where = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        if (f.apply(this, [this[i], i])) {
            res.push(this[i]);
        }
    }
    return res;
}

Array.prototype.select = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        res.push(s);
    }
    return res;
}

Array.prototype.selectMany = function(f) {
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        var s = f.apply(this, [this[i], i]);
        if (Array.isArray(s)) {
            for(var x = 0; x < s.length; x++)
                res.push(s[x]);
        }
    }
    return res;
}

Array.prototype.sum = function(f) {
    var res = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
    }
    return res;
}

Array.prototype.max = function(f) {
    var res = null;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        if (res == null) res = s;
        else res = Math.max(res, s);
    }
    return res;
}

Array.prototype.min = function(f) {
    var res = null;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        if (res == null) res = s;
        else res = Math.min(res, s);
    }
    return res;
}

Array.prototype.avg = function(f) {
    var res = 0;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (typeof f === "function")
            s = f.apply(this, [s, i]);
        res += s;
        cnt++;
    }
    return res / cnt;
}

Array.prototype.first = function() {
    return this[0];
}

Array.prototype.firstOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[0];
}

Array.prototype.last = function() {
    return this[this.length - 1];
}

Array.prototype.lastOrDefault = function(def) {
    if (this.length == 0) return def;
    return this[this.length - 1];
}

Array.prototype.count = function(f) {
    if (typeof f !== "function") return this.length;
    var cnt = 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
        cnt++;
    }
    return cnt;
}

Array.prototype.any = function(f) {
    if (typeof f !== "function") return this.length > 0;
    for(var i = 0; i < this.length; i++) {
        var s = this[i];
        if (f.apply(this, [s, i]))
            return true;
    }
    return false;
}

Array.prototype.pushRange = function(r) {
    if (!Array.isArray(r)) return this;
    for(var i = 0; i < r.length; i++)
        this.push(r[i]);
    return this;
}

Array.prototype.leftJoin = function(r, f) {
    if (!$a) throw new Error("This functionality requires alice.vanilla.objectManipulation module");
    if (!Array.isArray(r)) return [];
    if (typeof f !== "function") return [];
    var res = [];
    for(var i = 0; i < this.length; i++) {
        if (typeof this[i] != "object") continue;
        for(var j = 0; j < r.length; j++) {
            if (typeof r[j] != "object") continue;
            if (f.apply(this, [this[i], r[j]]))
                res.push($a.combineObject(this[i], r[j]));
        }
    }
    return res;
}
/*== alice.vanilla.objectManipulation.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};
$a.combineObject = function(a, b) {
    if (typeof a != 'object' || typeof b != "object")
        throw new Error("Both parameter must be an object");
    var res = $a.deepClone(a) || {};
    if (!b) return res;
    for(var k in b) {
        if (!res[k])
            res[k] = $a.deepClone(b[k]);
    }
    return res;
}
$a.shallowClone = function(a) {
    if (typeof a == "object") {
        if (Array.isArray(a)) {
            var r = [];
            for(var i = 0; i < a.length; i++)
                r.push(a[i]);
            return r;
        } 
        else if (a) {
            var r = {};
            for(var k in a) 
                r[k] = a[k];
            return r;
        }
        else
            return a;
    }
    return a;
}
$a.clone = function(a) {
    return $a.shallowClone(a);
}
$a.deepClone = function(a) {
    if (typeof a == "object") {
        if (Array.isArray(a)) {
            var r = [];
            for(var i = 0; i < a.length; i++)
                r.push($a.deepClone(a[i]));
            return r;
        } 
        else if (a) {
            var r = {};
            for(var k in a) 
                r[k] = $a.deepClone(a[k]);
            return r;
        }
        else
            return a;
    }
    return a;
}
/*== alice.jquery.ajax.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class AliceCRUDManager {
    constructor(mgr) {
        this._vm = mgr._vm;
        this._mgr = mgr;
        this._form = null;

        this.msgSaveSuccess = 'Saved successfully!';
        this.msgSaveFailed = 'Save failed!';
        this.msgDeleteSuccess = 'Deleted successfully!';
        this.msgDeleteFailed = 'Delete failed!';
    }

    validate(form) {
        form = form || this._form;
        var validator = $(form).kendoValidator().data("kendoValidator");
        return validator.validate();
    }

    setForm(form) {
        this._form = form;
    }

    save(url, data, skipValidation) {
        if (!skipValidation && !this.validate())
            return;
        
        if (data.isModel)
            data = data._unmap();
        this._mgr.post(url, data).then(function(res) {
            console.log(res);
        });
    }
}

class AliceAJAXManager {
    constructor(alice) {
        this._vm = alice;
        this.debug = false;
    };

    _pool = [];
    _id = 1;

    send(options) {
        var rec = new AliceAJAXRecord(options, this);
        rec.ID = this._id++;

        rec.start();
        $.ajax(options).then(
            function (data, textStatus, jqXHR) {
                this.done(data, textStatus, jqXHR);
            }.bind(rec),
            function (jqXHR, textStatus) {
                try {
                    this.done(JSON.parse(jqXHR.responseText), textStatus, jqXHR);
                }
                catch {
                    this.done(jqXHR.responseText, textStatus, jqXHR);
                }
            }.bind(rec)
        );

        this._pool.push(rec);
        return rec;
    };

    get(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'GET'
        };
        return this.send(opt);
    }

    post(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'POST'
        };
        return this.send(opt);
    }

    postJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'POST'
        };
        return this.send(opt);
    }

    postFormData(url, form) {
        var opt = {
            url: url,
            data: form,
            contentType: 'application/json; charset=utf-8',
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST'
        };
        return this.send(opt);
    }

    patch(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'PATCH'
        };
        return this.send(opt);
    }

    patchJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'PATCH'
        };
        return this.send(opt);
    }

    delete(url, data) {
        var opt = {
            url: url,
            data: data,
            method: 'DELETE'
        };
        return this.send(opt);
    }

    deleteJSON(url, data) {
        var opt = {
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            rawData: data,
            method: 'DELETE'
        };
        return this.send(opt);
    }
}

class AliceAJAXRecord {
    constructor(opt, mgr) {
        this.ID = -1;
        this.Url = opt.url;
        this.Method = opt.method;
        this.Initiated = new Date();
        this.Data = opt.rawData || opt.data;
        this._manager = mgr;
    }

    start() {
        this.Initiated = new Date();
        vm.debugInfo("Ajax request #" + this.ID + " is started: " + this.Method + " " + this.Url, this.Data);
    }

    then(f) {
        this._after = f;
    }

    done(data, textStatus, jqXHR) {
        if (typeof this._after === "function") {
            if (textStatus == "error")
                vm.debugError("Ajax request #" + this.ID + " error: " + this.Method + " " + this.Url, data);
            else {
                var el = new Date() - this.Initiated;
                vm.debugSuccess("Ajax request #" + this.ID + " is completed (" + el + "ms elapsed): " + this.Method + " " + this.Url, data);
            }
            this._after(data, textStatus, jqXHR);
        }
    }

    remove() {
        var ix = this._manager._pool.indexOf(this);
        if (ix >= 0)
            this._manager._pool.splice(ix, 1);
    }
}

$a.ajax = new AliceAJAXManager($a);
$a.crud = new AliceCRUDManager($a.ajax);
/*== alice.ko.dataManager.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class AliceModelManager {
    constructor(alice) {
        this._vm = alice;
    }

    add(name, model, addToData) {
        if (typeof name !== "string") {
            vm.debugError("A model name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            vm.debugError("A model named [" + name + "] already exists");
            return;
        }
        if (typeof model !== "object") {
            vm.debugError("A model must be an object, " + typeof model + " given");
            return;
        }
        this[name] = new AliceModelRecord(model);
        if (addToData) {
            this._vm.data.add(name, this[name]);
        }
    }
}

class AliceModelRecord {
    constructor(mdl) {
        this.base = mdl;
        this.isModel = true;
    }

    map() {
        var re = ko.mapping.fromJS(this.base);
        re._empty = this.empty.bind(this, re);
        re._unmap = this.unmap.bind(this, re);
        re._fill = this.fill.bind(this, re);
        return re;
    }

    empty(data) {
        ko.mapping.fromJS(this.base, data);
    }

    unmap(data) {
        return ko.mapping.toJS(data);
    }

    fill(data, record) {
        ko.mapping.fromJS(record, data);
    }
}

class AliceDataManager {
    constructor(alice) {
        this._vm = alice;
    }

    add(name, data) {
        if (typeof name !== "string") {
            console.error("A data name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            console.error("A data named [" + name + "] already exists");
            return;
        }
        if (Array.isArray(data)) {
            this[name] = ko.observableArray(data);
        }
        else if (data.isModel) {
            this[name] = data.map();
        }
        else {
            if (typeof data == "object")
                this[name] = ko.mapping.fromJS(data);
            else
                this[name] = ko.observable(data);
        }
    }

    fromModel(mname, name) {
        if (!name)
            name = mname;
        if (typeof name !== "string") {
            console.error("A data name must be a string, " + typeof name + " given");
            return;
        }
        if (this[name]) {
            console.error("A data named [" + name + "] already exists");
            return;
        }
        if (this._vm.models[mname] && typeof this._vm.models[mname] === "object") {
            this[name] = this._vm.models[mname].map();
        }
    }
}

$a.models = new AliceModelManager($a);
$a.data = new AliceDataManager($a);
$a.applyKnockoutBindings = function() {
    ko.applyBindings($a.data);
}
/*== alice.swal2.js by Code Atelier (c) 2021 ==*/
var $a = $a || {};

class SWALController {
    constructor(alice) {
        this._vm = alice;
    }

    success(message, title) {
        return Swal.fire(
            title,
            message,
            'success'
        );
    };
    info(message, title) {
        return Swal.fire(
            title,
            message,
            'info'
        );
    };
    error(message, title) {
        return Swal.fire(
            title,
            message,
            'error'
        );
    };
    warn(message, title, btnOK, btnCancel) {
        return Swal.fire({
            title: title || "",
            text: message || "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: btnOK || 'Yes',
            cancelButtonText: btnCancel || 'No',
        });
    };
}

$a.swal = new SWALController($a);
